#!/usr/bin/env python3

import	sys
from	math import *

def binomial(a, b):
	res = (factorial(a) / (factorial(b) * factorial(a-b))) * pow(1/6, b) * pow(5/6, a-b)
	return (res)

def calcProbability(c, nb):
	res = 0
	NbOccurence = nbList.count(int(nb))
	if NbOccurence >= c:
	 	res = 1
	else:
		for x in range(c - NbOccurence, 5 - NbOccurence + 1):			
			res += binomial(5 - NbOccurence, x)
	return res

def showResult(str, res):
	print("Chance to get a " + str + " : %0.2f%%" % (res * 100))

def paire(av):
	showResult("paire", calcProbability(2, int(av[1])))

def brelan(av):
	showResult("brelan", calcProbability(3, int(av[1])))

def full(av):
	res = 0
	if len(av) < 3:
		print("Not enough argument.")
		exit(-1)
	if av[1] == av[2]:
		print("Invalid argument given. Exiting program...")
		exit(-1)
	pass

def suite(av):
	pass

def carre(av):
	showResult("carre", calcProbability(4, int(av[1])))

def yams(av):
	showResult("yams", calcProbability(5, int(av[1])))

def launch(option):
	ptrFunc = {
	"paire" : paire,
	"brelan" : brelan,
	"carre" : carre,
	"full" : full,
	"suite" : suite,
	"yams" : yams,
	}

	for key in ptrFunc:
		if key == option[0]:
			ptrFunc[key](option)
			return
	
	print("Wrong options. Exiting now...")
	return

if len(sys.argv) != 7:
	print("./201yams <nb0> <nb1> <nb2> <nb3> <nb4> <option>")
else:
	nbList = list()
	option = sys.argv[6].split("_")

	if len(option) < 2:
		print("Not enough argument.")
		exit(-1)

# 	Check if nb is in [0;6] interval
	for x in range(1,6):
		if sys.argv[x].isdigit():
			nb = int(sys.argv[x])
			if (nb < 0 or nb > 6):
				print("Invalid argument given. Exiting program...")
				exit(-1)
			nbList.append(nb)
		else:
			print("Argument must be integer. Exiting program...")
			exit(-1)

#	Check if nb in option parameter is in [1;6] interval
	for x in range(1,len(option)):
		if option[x].isdigit():
			nb = int(option[x])
			if (nb <= 0 or nb > 6):
				print("Invalid argument given. Exiting program...")
				exit(-1)
		else:
			print("Argument must be integer. Exiting program...")
			exit(-1)

	launch(option)